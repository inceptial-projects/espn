package Cricketscoredetails;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CricinfoLivescorePage extends Initializer {

    protected List<String> url = new ArrayList<String>();
    protected ArrayList<WebElement> matches = (ArrayList<WebElement>) driver
            .findElements(By.xpath("//div[@class='match-score-block']/div[2]/a"));
    WebDriverWait wait = new WebDriverWait(driver, 5);

    public void getMatchLinks() {
        // System.out.println(matches.size());
        for (int i = 1; i <= matches.size(); i++) {
            String match = ((matches.get(i - 1).getAttribute("href")));
            url.add(match);
        }
    }
    public void clickMatchLinks() {
        for (int i = 0; i < 3; i++) {
            driver.navigate().to(url.get(i));
            //driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
            wait.until(ExpectedConditions
                    .visibilityOfElementLocated(By.xpath("//div[@class='match-header-container']")));
//			 wait.until(ExpectedConditions
//						.visibilityOfElementLocated(By.xpath("//div[@class='live-scorecard']/div[2]/div/div/div/div")));
            CricinfoMatchDetailsPage cmdp = new CricinfoMatchDetailsPage();
            cmdp.getMatchDetails();
            cmdp.toJson();
        }
    }


}
