package Cricketscoredetails;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class Initializer {

    static WebDriver driver;
    public void initDriver() {
        System.setProperty("webdriver.chrome.driver",
                "src/chromedriver_win32/chromedriver.exe");
        driver = new ChromeDriver();
    }

    public void navigateEspncricinfo() {
        driver.navigate().to("https://www.espncricinfo.com");
    }

    public String fetchPageTitle() {
        return driver.getTitle();
    }

    public void maximizeWindow() {
        driver.manage().window().maximize();
    }

    public void waitToLoad() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(50));
    }
    public void closeDriver() {
        driver.quit();
    }


}


